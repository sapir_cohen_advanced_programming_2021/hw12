#include "threads.h"
#include <Windows.h>
#include <thread>
#include <mutex>

std::mutex mtx;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool prime = true;
	if (file.is_open()) //checking if the file is open
	{
		for (int i = begin; i <= end; i++)
		{
			for (int j = 2; j <= std::sqrt(i) && prime; j++) //checking if i divides more numbers (checking prime number)
			{
				if (i % j == 0) prime = false;
			}
			mtx.lock();
			if (prime) file << i << std::endl; //writing to file if the number is prime
			mtx.unlock();
			prime = true;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	std::thread* threads = new std::thread[N];
	if (file.is_open())
	{
		std::chrono::high_resolution_clock::time_point start_t = std::chrono::high_resolution_clock::now();
		for (int i = 1; i <= N; i++)
		{
			threads[i-1] = std::thread(writePrimesToFile, begin + ((end - begin) /N) * (i - 1) , begin + ((end - begin) / N) * i, std::ref(file));
		}
		for (int i = 0; i < N; i++) threads[i].join();
		std::chrono::high_resolution_clock::time_point end_t = std::chrono::high_resolution_clock::now();
		std::cout << "Time: " << std::chrono::duration<double, std::milli>(end_t - start_t).count() << std::endl;
		file.close();
	}
	else std::cerr << "Could not open file!" << std::endl;
}