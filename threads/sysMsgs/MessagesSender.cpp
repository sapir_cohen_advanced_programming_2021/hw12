#include "MessagesSender.h"
#include <Windows.h>
#include <condition_variable>
#include <mutex>

std::condition_variable cv;
std::mutex mtxUsers;
std::mutex mtxQ;

MessagesSender::MessagesSender() {	}

MessagesSender::~MessagesSender()
{
	this->_users.clear();
}

void MessagesSender::signIn(std::string user)
{
	if (this->_users.find(user) != this->_users.end()) std::cerr << "ERROR! User already exists!" << std::endl;
	else
	{
		mtxUsers.lock();
		this->_users.insert(user);
		mtxUsers.unlock();
	}
}

void MessagesSender::signOut(std::string user)
{
	mtxUsers.lock();
	this->_users.erase(user);
	mtxUsers.unlock();
}

void MessagesSender::ConnectedUsers() const
{
	for (std::set<std::string>::iterator it = this->_users.begin(); it != this->_users.end(); ++it) std::cout << *it << std::endl;
}

void MessagesSender::sendMsgs(std::string fileName, std::queue<std::string>& qMsgs, MessagesSender& ms, bool& end_prog)
{
	std::ofstream file;
	while (!end_prog)
	{
		file.open(fileName, std::ios_base::app);
		std::unique_lock<std::mutex> mtxUl(mtxQ);
		cv.wait(mtxUl, [&]() {return !ms._users.empty() && !qMsgs.empty(); });
		while (!qMsgs.empty())
		{
			for (std::set<std::string>::iterator it = ms._users.begin(); it != ms._users.end(); ++it) file << *it << ": " << qMsgs.front() << std::endl;
			qMsgs.pop();
		}
		file.close();
		mtxUl.unlock();
	}
}

void MessagesSender::readFile(std::string fileName, std::queue<std::string>& qMsgs, bool& end_prog)
{
	std::string data;
	while (!end_prog)
	{
		std::ifstream file(fileName);
		std::unique_lock<std::mutex> mtxUlq(mtxQ);
		if (file.peek() != std::ifstream::traits_type::eof() && file.is_open()) while (getline(file, data)) qMsgs.push(data);
		file.close();
		
		//clearing data
		std::ofstream fileToClear(fileName);
		fileToClear.close();

		mtxUlq.unlock();

		cv.notify_one();
		std::this_thread::sleep_for(std::chrono::seconds(TIME_WAIT_M));
	}
}
