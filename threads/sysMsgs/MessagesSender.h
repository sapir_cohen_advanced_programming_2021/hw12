#pragma once

#include <iostream>
#include <set>
#include <queue>
#include <string>
#include <fstream>

#define TIME_WAIT_M 10

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void signIn(std::string user);
	void signOut(std::string user);
	void ConnectedUsers() const;
	
	static void sendMsgs(std::string fileName, std::queue<std::string>& qMsgs, MessagesSender& ms, bool& end_prog);
	static void readFile(std::string fileName, std::queue<std::string>& qMsgs, bool& end_prog);

private:
	std::set<std::string> _users;
};
