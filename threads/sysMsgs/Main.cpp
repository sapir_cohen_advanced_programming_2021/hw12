#include "MessagesSender.h"
#include <thread>

#define SIZE 3

#define INPUT_FILE_NAME "data.txt"
#define OUTPUT_FILE_NAME "output.txt"

enum choices{SIGN_IN = 1, SIGN_OUT, CONNECTED_USERS, EXIT};

int printMenu();
void runMsgsMenu();

std::queue<std::string> qMsgs;
MessagesSender ms;

bool end_progVal = false;
bool& end_prog = end_progVal;

int main()
{
	std::thread t[SIZE] = { std::thread(runMsgsMenu),
							std::thread(ms.readFile, INPUT_FILE_NAME, std::ref(qMsgs), std::ref(end_prog)),
							std::thread(ms.sendMsgs, OUTPUT_FILE_NAME, std::ref(qMsgs), std::ref(ms), std::ref(end_prog)) };

	for (int i = 0; i < SIZE; i++) t[i].join();

	return 0;
}

int printMenu()
{
	int choice = 0;

	std::cout << SIGN_IN << ". sign in." << std::endl;
	std::cout << SIGN_OUT << ". sign out." << std::endl;
	std::cout << CONNECTED_USERS << ". show connected users." << std::endl;
	std::cout << EXIT << ". exit." << std::endl;

	while (choice != SIGN_IN &&
		choice != SIGN_OUT &&
		choice != CONNECTED_USERS &&
		choice != EXIT)
	{
		std::cout << "make your choice: ";
		std::cin >> choice;
	}

	std::cout << std::endl;

	return choice;
}

void runMsgsMenu()
{
	int choice = 0;
	std::string input;
	while (choice != EXIT)
	{
		choice = printMenu();
		system("CLS");
		switch (choice)
		{
		case SIGN_IN:
			std::cout << "Enter user: ";
			std::cin >> input;
			std::cout << std::endl;
			ms.signIn(input);
			system("PAUSE");
			break;
		case SIGN_OUT:
			std::cout << "Enter user: ";
			std::cin >> input;
			std::cout << std::endl;
			ms.signOut(input);
			system("PAUSE");
			break;
		case CONNECTED_USERS:
			ms.ConnectedUsers();
			system("PAUSE");
			break;
		case EXIT:
			end_prog = true;
			exit(0);
			break;
		default:
			std::cout << "invalid choice!" << std::endl;
			system("PAUSE");
			break;
		}
		system("CLS");
	}
}
